#######################################################################################################################
########################################       INFORMATIONS       #####################################################
#######################################################################################################################

This is a web based app, it was designed to help Lannister Carriage Services provide its customers
information about the routes. It compute the distance along a certain route,
the number of different routes between two towns, and the shortest route between two towns.
---------------------
#ABOUT
This project was built using the following technologies:
Java 1.6
JSF Mojarra 2.1.6
jUnit 4
Tomcat 7
Primefaces 5.1
---------------------
#OTHERS JARS
el-ri-1.0.jar
jstl-1.2.jar
omnifaces-1.7.jar
weld-servlet-2.2.0.Final.jar
---------------------
#TO RUN THE TESTS
Run the class RouteBRTest
---------------------
#USING THE APPLICATION

To run the app, please follow the steps:
1) Start Tomcat
2) Go to the following URL: http://localhost:8080/CarriageServices/
3) A preloaded directed graph will appear in the inputtext, but you can change it any time you want
4) Hit the 'Load Graph' button
5) The table will be loaded with the directions and distance between each one, and three button will appear
6) Choose the action you want between 'Distance of route', 'Routes' or 'Shortest route'
7) A modal panel will appear
8) Type the desired routes, and ht the button 'Calculate'
9) A message will appear with the result

Any problem, contact: felipe.messina@gmail.com