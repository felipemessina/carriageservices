package com.avenuecode.util;

import java.util.ArrayList;
import java.util.List;

import com.avenuecode.model.Node;
/**
 * @author felipe.messina
 * @description Useful class from system
 *
 */
public class CSUtils {
	
	/**
	 * 
	 * @param path
	 * @return list with all the inputted paths and distance
	 */
	public static List<Node> splitPaths(String path){
		List<Node> list  = new ArrayList<Node>();
		String[] entries = path.split(Constants.SEPARATOR);
		String entry	 = Constants.EMPTY_STRING;
		Node node 		 = new Node();
		
		for (int i = Constants.VALUE_0; i < entries.length; i++) {
			entry = entries[i].trim();
			node.setStart(entry.substring(Constants.START, Constants.END));
			node.setEnd(entry.substring(Constants.END, Constants.DISTANCE));
			node.setDistance(Integer.valueOf(entry.substring(Constants.DISTANCE)));
			list.add(node);
			node = new Node();
			entry = Constants.EMPTY_STRING;
			
		}
		return list;
	}
	
	/**
	 * 
	 * @param path
	 * @return array of string with all the routes desired
	 */
	public static String[] splitRoutes(String path){
		return path.split(Constants.DASH);
	}
	
	/**
	 * 
	 * @param route
	 * @return validation result - true or false
	 */
	public static boolean validateInputedRoute(String route){
		String[] routes = CSUtils.splitRoutes(route);
		for (int i = Constants.VALUE_0; i < routes.length ; i++) {
			if(routes[i].length() > Constants.MAX_SIZE_ROUTE){
				return Boolean.FALSE;
			}
		}
		
		if(routes.length < Constants.MIN_ROUTES){
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
}