package com.avenuecode.util;

/**
 * @author felipe.messina
 * @description System constants
 *
 */
public class Constants {
	
	public static final String SEPARATOR 	= ",";
	public static final String COLON_SPACE 	= ": ";
	public static final String EMPTY_STRING = "";
	public static final String DASH 		= "-";
	
	public static final Integer	START 						= 0;
	public static final Integer END 						= 1;
	public static final Integer DISTANCE 					= 2;
	public static final Integer VALUE_0 					= 0;
	public static final Integer SHORTEST_ROUTE_INIT_VALUE 	= 999999999;
	public static final Integer MAX_STOPS 					= 3;
	public static final Integer ONE_STOP 					= 1;
	public static final Integer TWO_STOPS 					= 2;
	public static final Integer THREE_STOPS 				= 3;
	public static final Integer MIN_ROUTES 					= 2;
	public static final Integer MAX_SIZE_ROUTE				= 1;
	
	public static final String MSG_NO_SUCH_ROUTE 		= "NO SUCH ROUTE";
	public static final String FIELD_MSG_DISTANCE 		= "distance";
	public static final String FIELD_MSG_ROUTES 		= "routes";
	public static final String FIELD_MSG_SHORTEST_ROUTE = "shortestRouteMsg";
	
	public static final String STOP 	= " stop";
	public static final String STOPS 	= " stops";

}
