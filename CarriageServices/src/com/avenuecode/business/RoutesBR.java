package com.avenuecode.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avenuecode.model.Node;
import com.avenuecode.model.Routes;
import com.avenuecode.util.CSUtils;
import com.avenuecode.util.Constants;

/**
 * @author felipe.messina
 * @description Route's Business Rules
 *
 */
public class RoutesBR implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 544217104364388588L;

	/**
	 * 
	 * @param nodeList
	 * @param route
	 * @return distance
	 */
	public Integer calculateDistance(List<Node> nodeList, String route){
		
		String[] path = CSUtils.splitRoutes(route);

		Integer routePairs 		= path.length-1;
		Integer distance 		= Constants.VALUE_0;
		Integer routesComputed 	= Constants.VALUE_0;
		Integer pathPosition 	= Constants.VALUE_0;
		Integer nodesChecked 	= Constants.VALUE_0;
		String startPoint  		= path[pathPosition];
		String endPoint			= path[pathPosition+1];
		Boolean matchNode 		= Boolean.FALSE;
		
		
		while (routesComputed < routePairs) {
			routesComputed ++;
			for (Node node : nodeList) {
				nodesChecked++;

				if(startPoint.equals(node.getStart()) && endPoint.equals(node.getEnd())){
					distance  = distance + node.getDistance();
					matchNode = Boolean.TRUE;
					break;
				}
			}
			
			if(nodesChecked == nodeList.size() && !matchNode){
				distance = Constants.VALUE_0;
				break;
			}
			
			pathPosition++;
			if( pathPosition < routePairs){
				nodesChecked = Constants.VALUE_0;
				matchNode 	 = Boolean.FALSE;
				startPoint   = path[pathPosition];
				endPoint	 = path[pathPosition+1];
			}
		}
		return distance;
	}
	
	/**
	 * 
	 * @param fullList
	 * @param route
	 * @return map with directions and stops
	 */
	public Map<String, Integer> findRoutes(List<Node> fullList, Routes route){
		
		String startNode 	= route.getStartPoint();
		String endNode 		= route.getEndPoint();
		Integer maxStops 	= route.getMaxStops();
		
		List<Node> partialStartList 	= new ArrayList<Node>();
		List<Node> partialEndList 		= new ArrayList<Node>();
		Map<String, Integer> nodesMap 	= new HashMap<String, Integer>();
		
		for (Node node : fullList) {
			if(startNode.equals(node.getStart())){
				partialStartList.add(node);
			}
			if(endNode.equals(node.getEnd())){
				partialEndList.add(node);
			}
		}
		
		for (Node partialNode : partialStartList) {
			for (Node node : fullList) {
				//1stop
				if(startNode.equals(node.getStart()) && endNode.equals(node.getEnd())){
					nodesMap.put(startNode + Constants.DASH + endNode, Constants.ONE_STOP);
				}
				//2stops
				if(partialNode.getEnd().equals(node.getStart()) &&
						node.getEnd().equals(endNode)){
					nodesMap.put(startNode + Constants.DASH + node.getStart() + Constants.DASH + 
									endNode, Constants.TWO_STOPS);
				}
				
				if(maxStops > 2){
					for (Node endAux : partialEndList) {
						//3stops
						if(partialNode.getEnd().equals(node.getStart()) &&
								node.getEnd().equals(endAux.getStart())){
							nodesMap.put(startNode + Constants.DASH + partialNode.getEnd() + Constants.DASH + 
										 endAux.getStart() + Constants.DASH + endNode, Constants.THREE_STOPS);
						}
					}
				}
			}
		}
		return nodesMap;
	}
	
	/**
	 * 
	 * @param nodeList
	 * @param routesEntryShort
	 * @return string with the shortest route
	 */
	public String findShortestWay(List<Node> nodeList, Routes routesEntryShort){
		
		Integer shortestRoute = Constants.SHORTEST_ROUTE_INIT_VALUE;
		Integer routeDistance = Constants.VALUE_0;
		String route = Constants.EMPTY_STRING;
		String msg = Constants.EMPTY_STRING;
		
		routesEntryShort.setMaxStops(Constants.MAX_STOPS);
		Map<String, Integer> mapRoutes = findRoutes(nodeList, routesEntryShort);
		
		for (Map.Entry<String, Integer> entry : mapRoutes.entrySet()){
			routeDistance = calculateDistance(nodeList, entry.getKey());
			if(routeDistance < shortestRoute){
				shortestRoute = routeDistance;
				route 		  = entry.getKey();
			}
		}
		
		if(!mapRoutes.isEmpty()){
			msg = "Shortest route (by distance) from " + routesEntryShort.getStartPoint() + " to " +
					routesEntryShort.getEndPoint() + Constants.COLON_SPACE + route + 
						" ( distance = " + shortestRoute + " )";		
		}
		return msg;
	}

}