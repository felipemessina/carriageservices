package com.avenuecode.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.avenuecode.business.RoutesBR;
import com.avenuecode.model.Node;
import com.avenuecode.model.Routes;
import com.avenuecode.util.CSUtils;
import com.avenuecode.util.Constants;

/**
 * @author felipe.messina
 * @description Test class for Route's Business Rules
 *
 */
public class RouteBRTest {
	
	List<Node> list;
	RoutesBR routes;
	
	@Before
	public void init(){
		list = CSUtils.splitPaths("AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7");
		routes  = new RoutesBR();
	}
	
	@Test
	public void splithRoutes(){
		String path = "A-B-C-D";
		String[] expectedPath = {"A","B","C","D"};
		Assert.assertTrue(Arrays.equals(expectedPath,CSUtils.splitRoutes(path)));
	}
	
	@Test
	public void splithPaths(){
		String graph = "AB5, BC4, CD8, DC8, DE6"; 
		List<Node> listExpected = new ArrayList<Node>();
		Node expectedNode = new Node();
		
		expectedNode.setStart("A");
		expectedNode.setEnd("B");
		expectedNode.setDistance(5);
		listExpected.add(expectedNode);
		expectedNode = new Node();
		
		expectedNode.setStart("B");
		expectedNode.setEnd("C");
		expectedNode.setDistance(4);
		listExpected.add(expectedNode);
		expectedNode = new Node();
		
		expectedNode.setStart("C");
		expectedNode.setEnd("D");
		expectedNode.setDistance(8);
		listExpected.add(expectedNode);
		expectedNode = new Node();
		
		expectedNode.setStart("D");
		expectedNode.setEnd("C");
		expectedNode.setDistance(8);
		listExpected.add(expectedNode);
		expectedNode = new Node();

		expectedNode.setStart("D");
		expectedNode.setEnd("E");
		expectedNode.setDistance(6);
		listExpected.add(expectedNode);
		expectedNode = new Node();
		
		List<Node> listSplitted = CSUtils.splitPaths(graph);
		Assert.assertTrue(listExpected.containsAll(listSplitted));
	}
	
	@Test
	public void calculateABCDistance(){
		assertEquals("9", routes.calculateDistance(list, "A-B-C").toString());
	}
	
	@Test
	public void calculateADDistance(){
		assertEquals("5", routes.calculateDistance(list, "A-D").toString());
	}
	
	@Test
	public void calculateADCDistance(){
		assertEquals("13", routes.calculateDistance(list, "A-D-C").toString());
	}
	
	@Test
	public void calculateAEBCDDistance(){
		assertEquals("22", routes.calculateDistance(list, "A-E-B-C-D").toString());
	}
	
	@Test
	public void calculateAEDDistance(){
		assertEquals("0", routes.calculateDistance(list, "A-E-D").toString());
	}
	
	@Test
	public void calculateRoutesStartingCEndingC(){
		Map<String, Integer> mapExpected;
	    mapExpected = new HashMap<String, Integer>();
		mapExpected.put("C-D-C", 2);
		mapExpected.put("C-E-B-C", 3);
		
		Map<String, Integer> mapGot = new HashMap<String, Integer>();;
		Routes routesEntryRange = new Routes();
		routesEntryRange.setStartPoint("C");
		routesEntryRange.setEndPoint("C");
		routesEntryRange.setMaxStops(Constants.MAX_STOPS);
		mapGot = routes.findRoutes(list, routesEntryRange);
		
		Set<String> keysMapExpected = new HashSet<String>(mapExpected.keySet());
		Set<String> keysMapGot = new HashSet<String>(mapGot.keySet());
		
		Assert.assertTrue(keysMapExpected.equals(keysMapGot));
	}
	
	@Test
	public void calculateRoutesStartingAEndingC(){
		Map<String, Integer> mapExpected;
	    mapExpected = new HashMap<String, Integer>();
		mapExpected.put("A-B-C", 2);
		mapExpected.put("A-D-C", 2);
		mapExpected.put("A-E-B-C", 3);
		
		Map<String, Integer> mapGot = new HashMap<String, Integer>();;
		Routes routesEntryRange = new Routes();
		routesEntryRange.setStartPoint("A");
		routesEntryRange.setEndPoint("C");
		routesEntryRange.setMaxStops(Constants.MAX_STOPS);
		mapGot = routes.findRoutes(list, routesEntryRange);
		
		Set<String> keysMapExpected = new HashSet<String>(mapExpected.keySet());
		Set<String> keysMapGot = new HashSet<String>(mapGot.keySet());
		
		Assert.assertTrue(keysMapExpected.equals(keysMapGot));
	}
	
	@Test
	public void calculateShortestRouteFromAToC(){
		Routes routesEntryShort = new Routes();
		routesEntryShort.setStartPoint("A");
		routesEntryShort.setEndPoint("C");
		
		assertEquals("Shortest route (by distance) from A to C: A-B-C ( distance = 9 )", 
				routes.findShortestWay(list, routesEntryShort));
	}
	
	@Test
	public void calculateShortestRouteFromBToB(){
		Routes routesEntryShort = new Routes();
		routesEntryShort.setStartPoint("B");
		routesEntryShort.setEndPoint("B");
		
		assertEquals("Shortest route (by distance) from B to B: B-C-E-B ( distance = 9 )", 
				routes.findShortestWay(list, routesEntryShort));
	}
}