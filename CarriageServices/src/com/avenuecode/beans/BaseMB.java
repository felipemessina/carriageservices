package com.avenuecode.beans;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * 
 * @author felipe.messina
 * @description Base class for the managed beans
 *
 */
public class BaseMB {
	
	public void addInfoMessage(String field, String summary, String detail ){
		FacesContext.getCurrentInstance().addMessage(field, new FacesMessage(FacesMessage.SEVERITY_INFO, summary , detail));
	}
	
	public void addErrorMessage(String field, String summary, String detail ){
		FacesContext.getCurrentInstance().addMessage(field, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary , detail));
	}
	

}
