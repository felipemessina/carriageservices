package com.avenuecode.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.avenuecode.business.RoutesBR;
import com.avenuecode.model.Node;
import com.avenuecode.model.Routes;
import com.avenuecode.util.CSUtils;
import com.avenuecode.util.Constants;

/**
 * @author felipe.messina
 * @description Data Entry class
 *
 */
@Named
@ViewScoped
public class DataEntryMB extends BaseMB implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7297577431418237949L;
	
	private String directedGraph;
	private List<Node> nodeList;
	
	private String route;
	private Integer routeDistance;
	
	private Routes routesEntryRange = new Routes();
	private Routes routesEntryShort = new Routes();
	
	@Inject
	private RoutesBR routes;
	
	@PostConstruct
	public void init(){
		routesEntryRange.setMaxStops(Constants.THREE_STOPS);
		directedGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
	}
	
	public void load(){
		nodeList = CSUtils.splitPaths(directedGraph);
	}
	
	public void calculateDistance(){
		
		if(CSUtils.validateInputedRoute(route)){
		
			routeDistance = routes.calculateDistance(nodeList, route);
			
			if(routeDistance == Constants.VALUE_0 ){
				addErrorMessage(Constants.FIELD_MSG_DISTANCE, null, Constants.MSG_NO_SUCH_ROUTE);
			}else {				
				addInfoMessage(Constants.FIELD_MSG_DISTANCE, "Distance of route " + route + Constants.COLON_SPACE,
						routeDistance.toString());
			}
		}else{
			addErrorMessage(Constants.FIELD_MSG_DISTANCE, null, Constants.MSG_NO_SUCH_ROUTE);
		}
	}
	
	public void calculateRoutesBetweenPoints(){
		Map<String, Integer> mapRoutes;
		mapRoutes = routes.findRoutes(nodeList, routesEntryRange);
		
		for (Map.Entry<String, Integer> entry : mapRoutes.entrySet()){
			
			addInfoMessage(Constants.FIELD_MSG_ROUTES, entry.getKey() + Constants.COLON_SPACE,
					entry.getValue().toString() + (entry.getValue() == Constants.ONE_STOP ? Constants.STOP : Constants.STOPS));
		}
		
		if(mapRoutes.isEmpty()){
			addErrorMessage(Constants.FIELD_MSG_ROUTES, null, Constants.MSG_NO_SUCH_ROUTE);
		}
			
	}
	
	public void calculateShortestRoute(){
		String msg = routes.findShortestWay(nodeList, routesEntryShort);
		
		if(msg.isEmpty()){
			addErrorMessage(Constants.FIELD_MSG_SHORTEST_ROUTE, null, Constants.MSG_NO_SUCH_ROUTE);
		}else{	
			addInfoMessage(Constants.FIELD_MSG_SHORTEST_ROUTE, null, msg);
		}
		
	}
	
	public void setRouteDistance(Integer routeDistance) {
		this.routeDistance = routeDistance;
	}
	
	public String getDirectedGraph() {
		return directedGraph;
	}

	public void setDirectedGraph(String directedGraph) {
		this.directedGraph = directedGraph;
	}

	public List<Node> getNodeList() {
		return nodeList;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Integer getRouteDistance() {
		return routeDistance;
	}

	public Routes getRoutesEntryRange() {
		return routesEntryRange;
	}

	public void setRoutesEntryRange(Routes routesEntryRange) {
		this.routesEntryRange = routesEntryRange;
	}

	public Routes getRoutesEntryShort() {
		return routesEntryShort;
	}

	public void setRoutesEntryShort(Routes routesEntryShort) {
		this.routesEntryShort = routesEntryShort;
	}


}
