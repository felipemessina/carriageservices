package com.avenuecode.model;

/**
 * @author felipe.messina
 * @description Route's model
 *
 */
public class Routes {
	
	private String startPoint;
	private String endPoint;
	private Integer maxStops;
	
	public String getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(String startPoint) {
		this.startPoint = startPoint;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public Integer getMaxStops() {
		return maxStops;
	}
	public void setMaxStops(Integer maxStops) {
		this.maxStops = maxStops;
	}
}
